
# Testando API REST em Cypress

Este repositório contém exemplos e informações sobre como realizar testes automatizados de API REST usando o framework de teste de front-end Cypress. Os testes de API são uma parte essencial da garantia de qualidade de software, permitindo validar a funcionalidade e a integração das APIs de maneira automatizada e consistente.

## Pré-requisitos

- Node.js e npm instalados no seu sistema. [Baixar Node.js](https://nodejs.org/)
- Git instalado no seu sistema. [Baixar Git](https://git-scm.com/)

## Configuração do Ambiente

1. Clone este repositório para o seu ambiente local:

   
   git clone https://gitlab.com/marcos035/testando-api-rest-em-cypress.git


   
   Acesse o diretório do projeto:
   
   cd testando-api-rest-em-cypress

   Instale as dependências do projeto:
    
   npm install


