// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })






Cypress.Commands.add('postUser', (user) => {
    cy.api({
        url: '/users',
        method: 'POST',
        failOnStatusCode: false,
        body: user
    }).then(response => { return response })


})

Cypress.Commands.add('postSession', (user) => {
    cy.api({
        url: 'sessions',
        method: 'POST',
        body: { email: user.email, password: user.password },
        failOnStatusCode: false
    }).then(response => { return response })

})

Cypress.Commands.add('postTask', (task, token) => {

    cy.api({
        url: 'tasks',
        method: 'POST',
        body: task,
        headers: {
            Authorization: token
        },
        failOnStatusCode: false

    }).then(response => {
        return response
    })


})

Cypress.Commands.add('getTasks', (token) => {

    cy.api({

        url: '/tasks/',
        method: 'GET',
        headers: {
            authorization: token
        },
        failOnStatusCode: false

    }).then(response => {
        return response
    })
})



Cypress.Commands.add('getUniqueTask', (taskId, token) => {
    cy.api({

        url: '/tasks/' + taskId,
        method: 'GET',
        headers: {
            authorization: token
        },
        failOnStatusCode: false

    }).then(response => {
        return response
    })

})

Cypress.Commands.add('deleteTask', (taskId, token) => {
    cy.api({

        url: 'tasks/' + taskId,
        method: 'DELETE',
        headers: {
            authorization: token
        },
        failOnStatusCode: false

    }).then(response => {
        return response
    })

})
Cypress.Commands.add('putTask', (taskid, token) => {
    cy.api({
        url: `tasks/${taskid}/done`,
        method: 'PUT',
        headers: {
            Authorization:
                token
        },
        failOnStatusCode: false
    }).then(response => {
        return (response)
    })

   
})

 // estudos
 Cypress.Commands.add('criaConta',(name,email,password)=>{
    cy.visit('/')
    cy.get('a[href*="/"]').click()
    cy.get('input[placeholder*="Nome"]').type(name)
    cy.get('input[placeholder*="E-mail"]').type(email)
    cy.get('input[placeholder*="Senha"]').type(password)
    cy.get('button[type*="submit"]').click()
    .wait(1000)
    cy.get('#root > div > div.sc-fznyAO.CWQMf > div > form > div:nth-child(2) > div > div > p').should('not.have.text','Oops! Já existe um cadastro com e-mail informado.')
    .should('to.have.text','Boas vindas ao Mark85, o seu gerenciador de tarefas.')

    // arrumar com expect
    cy.contains('Voltar para login').click()

})

Cypress.Commands.add('logando', (email, password)=>{

    cy.get('input[placeholder*="E-mail"]').type(email)
    cy.get('input[placeholder*="Senha"]').type(password)
    cy.get('button[type*="submit"]').click()
    cy.get('section>div>h1').invoke('text').then((textoDoH1) => {
        cy.log(textoDoH1)
        expect(textoDoH1).to.eq('Minhas tarefas')
      });


})