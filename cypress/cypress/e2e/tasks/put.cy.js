describe('PUT/ tasks/ :id/done', () => {

    beforeEach(function () {
        cy.fixture('tasks/put').then(function (tasks) {
            this.tasks = tasks

        })
    })

    it('update task to done', function () {

        const { user, tasks } = this.tasks.update

        cy.task('removeTask', tasks.name, user.email)
        cy.task('removeUser', user.email)
        cy.postUser(user)

        cy.postSession(user)
            .then(userResp => {

                cy.postTask(tasks, userResp.body.token)
                    .then(taskResp => {

                        cy.putTask(taskResp.body._id, userResp.body.token)
                        .then(response => {
                            expect(response.status).to.eq(204)
                        })

                        cy.getUniqueTask(taskResp.body._id, userResp.body.token)
                        .then(response => {
                            expect(response.body.is_done).to.true
                            expect(response.status).to.eq(200)
                            

                        })

                    })
            })
    })


    it('not found put', function () {

        const { user, tasks } = this.tasks.not_found

        cy.task('removeTask', tasks.name, user.email)
        cy.task('removeUser', user.email)
        cy.postUser(user)

        cy.postSession(user)
            .then(userResp => {

                cy.postTask(tasks, userResp.body.token)
                    .then(taskResp => {

                        cy.deleteTask(taskResp.body._id, userResp.body.token)
                        .then(response => {
                            expect(response.status).to.eq(204)


                        })


                         cy.putTask(taskResp.body._id, userResp.body.token)
                         .then(response => {
                            expect(response.status).to.eq(404)


                        })
                    })
            })
    })
})


