
describe('Automação WEB', () => {

    // criar conta, login e criar tasks
    // montar descrição em gherkin
    beforeEach(function () {
        cy.fixture('estudo/web').then(function (estudo) {
            this.estudo = estudo
        })
    })

    it('Criando Login', function () {

        const { user } = this.estudo.web

        cy.task('removeUser', user.email)

        cy.criaConta(user.name, user.email, user.password)

    })

    it('Logando na aplicação', function () {

        const { userLogin } = this.estudo.web

        cy.task('removeUser', userLogin.email)

        cy.criaConta(userLogin.name, userLogin.email, userLogin.password)

        cy.logando(userLogin.email, userLogin.password)
    })

    it('Criando tasks', function () {

        const { loginTasks, tasks } = this.estudo.web

        cy.task('removeUser', loginTasks.email)

        cy.criaConta(loginTasks.name, loginTasks.email, loginTasks.password)

        cy.logando(loginTasks.email, loginTasks.password)

        cy.get('a[href*="/create"]').click()

        cy.get('input[placeholder*="Nome da tarefa"]').type(tasks.name)

        tasks.tags.forEach(tags => {
            cy.get('input[placeholder*="Informe até 3 tags"]').type(`${tasks.tags}{enter}`)
            //arrumar esse forEach para executar 3 tags ou inves de uma so 
        });
        cy.contains('button[type*="submit"]', 'Cadastrar').click()
        cy.get('section>div>h1').invoke('text').then((textoDoH1) => {
            cy.log(textoDoH1)
            expect(textoDoH1).to.eq('Minhas tarefas')
        });


    })

    //delete tasks

    // concluindo tasks 
    it('concluindo tasks', function () {

        const { loginTasksDone, tasks } = this.estudo.web

        cy.task('removeUser', loginTasksDone.email)

        cy.criaConta(loginTasksDone.name, loginTasksDone.email, loginTasksDone.password)

        cy.logando(loginTasksDone.email, loginTasksDone.password)

        cy.get('a[href*="/create"]').click()

        cy.get('input[placeholder*="Nome da tarefa"]').type(tasks.name)

        tasks.tags.forEach(tags => {
            cy.get('input[placeholder*="Informe até 3 tags"]').type(`${tasks.tags}{enter}`)
            //arrumar esse forEach para executar 3 tags ou inves de uma so 
        });
        cy.contains('button[type*="submit"]', 'Cadastrar').click()
        cy.get('section>div>h1').invoke('text').then((textoDoH1) => {
            cy.log(textoDoH1)
            expect(textoDoH1).to.eq('Minhas tarefas')
        });

        cy.get('.item-toggle').click()
        cy.get('.task-done').should('to.exist')




        // criando tasks  ok
        // concluindo tasks 
        // deletando tasks

    })



    //deletando tasks
    it('Deletando tasks', function () {

        const { loginTasksDone, tasks } = this.estudo.web

        cy.task('removeUser', loginTasksDone.email)

        cy.criaConta(loginTasksDone.name, loginTasksDone.email, loginTasksDone.password)

        cy.logando(loginTasksDone.email, loginTasksDone.password)

        cy.get('a[href*="/create"]').click()

        cy.get('input[placeholder*="Nome da tarefa"]').type(tasks.name)

        tasks.tags.forEach(tags => {
            cy.get('input[placeholder*="Informe até 3 tags"]').type(`${tasks.tags}{enter}`)
            //arrumar esse forEach para executar 3 tags ou inves de uma so 
        });
        cy.contains('button[type*="submit"]', 'Cadastrar').click()
        cy.get('section>div>h1').invoke('text').then((textoDoH1) => {
            cy.log(textoDoH1)
            expect(textoDoH1).to.eq('Minhas tarefas')
        });

        cy.get('.item-toggle').click()
        cy.get('.task-done').should('to.exist')

        cy.get('button[class*="task-remove"]').click()
        .should('to.not.exist')

        cy.get('.task-item').should('to.not.exist')




    })

})
describe('Automação API', () => {

    // criar conta, login e criar tasks
    // montar descrição em gherkin

})